package com.aconex.hometime;

import org.junit.Test;

import static org.junit.Assert.*;

public class TramsDateConverterTest {

  @Test
  public void shouldReturnAPlaceholderWhenGivenAnEmptyDateString() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("");

    // then
    assertEquals("???", actual);
  }

  @Test
  public void shouldReturnAPlaceholderWhenGivenAMalformattedDateString() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("dskfhsdjh");

    // then
    assertEquals("???", actual);
  }

  @Test
  public void shouldReturnAPlaceholderWhenDateStringMissingTheTimezone() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("/Date(1426821588000)/");

    // then
    assertEquals("???", actual);
  }

  @Test
  public void shouldReturnAPlaceholderWhenInternalNumberIsNotAUnixTimestamp() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("/Date(14268abcd88000+900)/");

    // then
    assertEquals("???", actual);
  }

  @Test
  public void shouldReturnFormattedTimeWhenGivenACorrectlyFormattedDateString() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("/Date(1426821588000+1100)/");

    // then
    assertEquals("2:19 PM", actual);
  }

  @Test
  public void shouldFormatTwoDigitsForHoursWhenGivenATimeAt11AtNight() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("/Date(1426853988000-500)/");

    // then
    assertEquals("11:19 PM", actual);
  }

  @Test
  public void shouldFormatAMForHoursWhenGivenATimeAt11InTheMorning() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("/Date(1426810788000-500)/");

    // then
    assertEquals("11:19 AM", actual);
  }

  @Test
  public void shouldIgnoreTimezoneWhenFormattingTheTimeAndAssumeLocalTimezone() throws Exception {
    // when
    String actual = TramsDateConverter.formattedTimeFromString("/Date(1426821588000-500)/");

    // then
    assertEquals("2:19 PM", actual);
  }
}
