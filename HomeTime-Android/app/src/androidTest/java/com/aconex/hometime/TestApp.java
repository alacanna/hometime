package com.aconex.hometime;

import com.jakewharton.espresso.OkHttp3IdlingResource;

public class TestApp extends App {

  private OkHttp3IdlingResource okHttpIdlingResource;

  @Override
  public void onCreate() {
    super.onCreate();

    okHttpIdlingResource = OkHttp3IdlingResource.create("OkHttp", getOkHttpClient());
  }

  public OkHttp3IdlingResource getOkHttpIdlingResource() {
    return okHttpIdlingResource;
  }
}
