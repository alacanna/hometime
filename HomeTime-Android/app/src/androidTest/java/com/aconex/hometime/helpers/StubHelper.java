package com.aconex.hometime.helpers;

import com.aconex.hometime.StubInterceptor;

import java.util.Locale;

import static com.aconex.hometime.StubInterceptor.getMinutesFromNowDateString;
import static java.net.HttpURLConnection.HTTP_OK;

public class StubHelper {

  public static void simulateDeviceTokenNetworkErrorResponse() {
    StubInterceptor stubInterceptor = StubInterceptor.getInstance();
    stubInterceptor.clearResponses();

    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetDeviceToken.*",
      -1,
      null, "Device token error"));
  }

  public static void simulateSuccesfulTramTimesNetworkResponse() {
    StubInterceptor stubInterceptor = StubInterceptor.getInstance();
    stubInterceptor.clearResponses();

    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetDeviceToken.*",
      HTTP_OK,
      "{\"responseObject\":[{\"DeviceToken\":\"some-token\"}]}\n", null));

    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetNextPredictedRoutesCollection.*3304.*",
      HTTP_OK,
      String.format(Locale.ENGLISH,
        "{\"responseObject\":[{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}" +
          ",{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}]}\n",
        getMinutesFromNowDateString(-60),
        getMinutesFromNowDateString(-5),
        getMinutesFromNowDateString(5),
        getMinutesFromNowDateString(24 * 60 - 10),
        getMinutesFromNowDateString(24 * 60 + 10)), null));

    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetNextPredictedRoutesCollection.*3204.*",
      HTTP_OK,
      String.format(Locale.ENGLISH,
        "{\"responseObject\":[{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}" +
          ",{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}]}\n",
        getMinutesFromNowDateString(-1),
        getMinutesFromNowDateString(90),
        getMinutesFromNowDateString(3 * 24 * 60),
        getMinutesFromNowDateString(7 * 24 * 60 + 10)), null));
  }

  public static void simulateTramNetworkErrorResponse() {
    StubInterceptor stubInterceptor = StubInterceptor.getInstance();
    stubInterceptor.clearResponses();

    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetDeviceToken.*",
      HTTP_OK,
      "{\"responseObject\":[{\"DeviceToken\":\"some-token\"}]}\n", null));

    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetNextPredictedRoutesCollection.*3304.*",
      -1, null, "East trams error"));
    stubInterceptor.addResponse(new StubInterceptor.FakeResponse(".*GetNextPredictedRoutesCollection.*3204.*",
      -1, null, "West trams error"));
  }
}
