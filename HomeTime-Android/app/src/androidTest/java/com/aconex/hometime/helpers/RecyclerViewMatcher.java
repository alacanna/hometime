package com.aconex.hometime.helpers;

import android.content.res.Resources;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class RecyclerViewMatcher {

  private final int recyclerViewId;

  public static RecyclerViewMatcher withRecyclerView(@IdRes int recyclerViewId) {
    return new RecyclerViewMatcher(recyclerViewId);
  }

  public RecyclerViewMatcher(@IdRes int recyclerViewId) {
    this.recyclerViewId = recyclerViewId;
  }

  public Matcher<View> atPosition(int position) {
    return atPositionOnView(position, -1);
  }

  public Matcher<View> atPositionOnView(final int position, @IdRes final int targetViewId) {
    return new TypeSafeMatcher<View>() {
      Resources resources = null;
      View childView;

      public void describeTo(Description description) {
        String idDescription = Integer.toString(recyclerViewId);
        if (resources != null) {
          try {
            idDescription = resources.getResourceName(recyclerViewId);
          } catch (Resources.NotFoundException exception) {
            idDescription = String.format("%s (resource name not found)", recyclerViewId);
          }
        }

        description.appendText("RecyclerView with id: " + idDescription + " at position: " + position);
      }

      public boolean matchesSafely(View view) {
        resources = view.getResources();

        if (childView == null) {
          RecyclerView recyclerView =
            (RecyclerView) view.getRootView().findViewById(recyclerViewId);
          if (recyclerView != null && recyclerView.getId() == recyclerViewId) {
            RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
            if (viewHolder != null) {
              childView = viewHolder.itemView;
            }
          } else {
            return false;
          }
        }

        if (targetViewId == -1) {
          return view == childView;
        } else {
          View targetView = childView.findViewById(targetViewId);
          return view == targetView;
        }
      }
    };
  }
}
