package com.aconex.hometime;

import android.support.test.espresso.Espresso;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.aconex.hometime.MainActivity;
import com.aconex.hometime.helpers.RecyclerViewItemCountAssertion;
import com.jakewharton.espresso.OkHttp3IdlingResource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.aconex.hometime.helpers.RecyclerViewMatcher.withRecyclerView;
import static com.aconex.hometime.helpers.StubHelper.simulateDeviceTokenNetworkErrorResponse;
import static com.aconex.hometime.helpers.StubHelper.simulateSuccesfulTramTimesNetworkResponse;
import static com.aconex.hometime.helpers.StubHelper.simulateTramNetworkErrorResponse;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainTest {

  private OkHttp3IdlingResource okHttpIdlingResource;

  @Rule
  public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

  @Before
  public void setUp() {
    okHttpIdlingResource = ((TestApp) activityTestRule.getActivity().getApplication()).getOkHttpIdlingResource();
  }

  @Test
  public void appHasExpectedTitleAndControls() {
    onView(withText("Home Time Trams")).check(matches(isDisplayed()));

    onView(withText("Load")).check(matches(isDisplayed()));
    onView(withText("Clear")).check(matches(isDisplayed()));

    onView(withText("Towards Parliament")).check(matches(isDisplayed()));
    onView(withText("Towards Southern Cross")).check(matches(isDisplayed()));
  }

  @Test
  public void appInitiallyShowsEmptyState() {
    onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(4));

    onView(withRecyclerView(R.id.recyclerView).atPosition(1))
      .check(matches(withText("No upcoming trams. Tap load to fetch")));

    onView(withRecyclerView(R.id.recyclerView).atPosition(3))
      .check(matches(withText("No upcoming trams. Tap load to fetch")));
  }

  @Test
  public void appShowsLoadingIndicatorsWhileLoading() {
    onView(withText("Load")).perform(click());

    simulateSuccesfulTramTimesNetworkResponse();

    onView(withRecyclerView(R.id.recyclerView).atPosition(1))
      .check(matches(withText("Loading upcoming trams…")));

    onView(withRecyclerView(R.id.recyclerView).atPosition(3))
      .check(matches(withText("Loading upcoming trams…")));
  }

  @Test
  public void appShouldDisplayErrorWhenTokenFailsToLoad() {
    Espresso.registerIdlingResources(okHttpIdlingResource);

    simulateDeviceTokenNetworkErrorResponse();

    onView(withText("Load")).perform(click());

    onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(4));

    onView(withRecyclerView(R.id.recyclerView).atPosition(1))
      .check(matches(withText("Device token error")));

    onView(withRecyclerView(R.id.recyclerView).atPosition(3))
      .check(matches(withText("Device token error")));

    Espresso.unregisterIdlingResources(okHttpIdlingResource);
  }

  @Test
  public void appShouldDisplayErrorWhenTramFailsToLoad() {
    Espresso.registerIdlingResources(okHttpIdlingResource);

    simulateTramNetworkErrorResponse();

    onView(withText("Load")).perform(click());

    onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(4));

    onView(withRecyclerView(R.id.recyclerView).atPosition(1))
      .check(matches(withText("East trams error")));

    onView(withRecyclerView(R.id.recyclerView).atPosition(3))
      .check(matches(withText("West trams error")));

    Espresso.unregisterIdlingResources(okHttpIdlingResource);
  }

  @Test
  public void appShowsTramTimesAfterLoading() {
    Espresso.registerIdlingResources(okHttpIdlingResource);

    simulateSuccesfulTramTimesNetworkResponse();

    onView(withText("Load")).perform(click());

    onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(11));

    onView(withRecyclerView(R.id.recyclerView).atPosition(1))
      .check(matches(withText("8:00 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(2))
      .check(matches(withText("8:55 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(3))
      .check(matches(withText("9:05 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(4))
      .check(matches(withText("8:50 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(5))
      .check(matches(withText("9:10 AM")));

    onView(withRecyclerView(R.id.recyclerView).atPosition(7))
      .check(matches(withText("8:59 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(8))
      .check(matches(withText("10:30 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(9))
      .check(matches(withText("9:00 AM")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(10))
      .check(matches(withText("9:10 AM")));

    Espresso.unregisterIdlingResources(okHttpIdlingResource);
  }

  @Test
  public void appShowsEmptyStateAfterClear() {
    Espresso.registerIdlingResources(okHttpIdlingResource);

    simulateSuccesfulTramTimesNetworkResponse();

    onView(withText("Load")).perform(click());
    onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(11));

    onView(withText("Clear")).perform(click());
    onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(4));
    onView(withRecyclerView(R.id.recyclerView).atPosition(1))
      .check(matches(withText("No upcoming trams. Tap load to fetch")));
    onView(withRecyclerView(R.id.recyclerView).atPosition(3))
      .check(matches(withText("No upcoming trams. Tap load to fetch")));

    Espresso.unregisterIdlingResources(okHttpIdlingResource);
  }

  /*
    This test will only pass once the extension exercise is done, uncomment to try it out

    @Test
    public void appShowsFormattedTramTimesAfterLoading() {
      Espresso.registerIdlingResources(okHttpIdlingResource);

      simulateSuccesfulTramTimesNetworkResponse();

      onView(withText("Load")).perform(click());

      onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(9));

      // Dates relative to 9:00 AM Friday, because we're using a fake reference date

      onView(withRecyclerView(R.id.recyclerView).atPosition(1))
        .check(matches(withText("8:55 AM")));
      onView(withRecyclerView(R.id.recyclerView).atPosition(2))
        .check(matches(withText("9:05 AM, in 5 min")));
      onView(withRecyclerView(R.id.recyclerView).atPosition(3))
        .check(matches(withText("8:50 AM, in 23 hours")));
      onView(withRecyclerView(R.id.recyclerView).atPosition(4))
        .check(matches(withText("9:10 AM, Saturday")));

      onView(withRecyclerView(R.id.recyclerView).atPosition(7))
        .check(matches(withText("8:59 AM")));
      onView(withRecyclerView(R.id.recyclerView).atPosition(8))
        .check(matches(withText("10:30 AM, in 1 hour")));
      onView(withRecyclerView(R.id.recyclerView).atPosition(9))
        .check(matches(withText("9:00 AM, Monday")));

      Espresso.unregisterIdlingResources(okHttpIdlingResource);
    }
  */
}
