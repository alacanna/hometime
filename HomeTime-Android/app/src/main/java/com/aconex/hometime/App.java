package com.aconex.hometime;

import android.app.Application;

import okhttp3.OkHttpClient;

public class App extends Application {

  private OkHttpClient okHttpClient;

  @Override
  public void onCreate() {
    super.onCreate();

    OkHttpClient.Builder builder = new OkHttpClient.Builder();

    if (BuildConfig.USE_NETWORK_STUB) {
      builder.addInterceptor(StubInterceptor.getInstance());
    }

    okHttpClient = builder.build();
  }

  public OkHttpClient getOkHttpClient() {
    return okHttpClient;
  }
}
