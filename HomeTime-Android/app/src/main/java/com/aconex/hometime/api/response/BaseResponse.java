package com.aconex.hometime.api.response;

import java.util.List;

/**
 * Created by amandalacanna on 8/31/17.
 */

public class BaseResponse<T> {
  private List<T> responseObject;

  public BaseResponse() {
  }

  public List<T> getResponseObject() {
    return responseObject;
  }
}
