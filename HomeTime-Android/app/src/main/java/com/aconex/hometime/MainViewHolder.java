package com.aconex.hometime;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

public class MainViewHolder extends RecyclerView.ViewHolder {

  private final TextView textView;

  public MainViewHolder(TextView itemView) {
    super(itemView);
    textView = itemView;
  }

  public void setHeadingText(String value) {
    textView.setText(value);
    textView.setTypeface(Typeface.DEFAULT_BOLD);
    textView.setTextColor(Color.BLACK);
  }

  public void setErrorText(String value) {
    textView.setText(value);
    textView.setTypeface(Typeface.DEFAULT);
    textView.setTextColor(Color.RED);
  }

  public void setPlaceholderText(String value) {
    textView.setText(value);
    textView.setTypeface(Typeface.DEFAULT);
    textView.setTextColor(Color.GRAY);
  }

  public void showFormattedTramDate(String value) {
    textView.setText(TramsDateConverter.formattedTimeFromString(value));
    textView.setTypeface(Typeface.DEFAULT);
    textView.setTextColor(Color.BLACK);
  }
}
