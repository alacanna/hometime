package com.aconex.hometime;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.aconex.hometime.R;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends SectionedRecyclerViewAdapter<MainViewHolder> {

  private final Context context;

  private List<String> eastTrams = new ArrayList<>();
  private List<String> westTrams = new ArrayList<>();
  private boolean isEastLoading = false;
  private boolean isWestLoading = false;
  private String eastErrorMessage = null;
  private String westErrorMessage = null;

  public MainAdapter(Context context) {
    this.context = context;
  }

  @Override
  public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View itemView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
    return new MainViewHolder((TextView) itemView);
  }

  @Override
  public int getSectionCount() {
    return 2;
  }

  @Override
  public int getItemCount(int section) {
    if (section == 0) return eastTrams.isEmpty() ? 1 : eastTrams.size();
    return westTrams.isEmpty() ? 1 : westTrams.size();
  }

  @Override
  public void onBindHeaderViewHolder(MainViewHolder holder, int section) {
    holder.setHeadingText(section == 0
      ? context.getString(R.string.towards_parliament) :
      context.getString(R.string.towards_southern_cross));
  }

  @Override
  public void onBindViewHolder(MainViewHolder holder, int section, int relativePosition, int absolutePosition) {
    List<String> trams = tramsForSection(section);
    if (!trams.isEmpty()) {
      holder.showFormattedTramDate(trams.get(relativePosition));
    } else if (isLoadingSection(section)) {
      holder.setPlaceholderText(context.getString(R.string.loading));
    } else if (!TextUtils.isEmpty(errorForSection(section))) {
      holder.setErrorText(errorForSection(section));
    } else {
      holder.setPlaceholderText(context.getString(R.string.tap_load_to_fetch));
    }
  }

  private String errorForSection(int section) {
    return section == 0 ? eastErrorMessage : westErrorMessage;
  }

  private boolean isLoadingSection(int section) {
    return section == 0 ? isEastLoading : isWestLoading;
  }

  private List<String> tramsForSection(int section) {
    return section == 0 ? eastTrams : westTrams;
  }

  public void setEastTrams(List<String> eastTrams) {
    this.eastTrams = eastTrams;
  }

  public void setWestTrams(List<String> westTrams) {
    this.westTrams = westTrams;
  }

  public void clearAllData() {
    isEastLoading = false;
    isWestLoading = false;
    eastTrams.clear();
    westTrams.clear();
    eastErrorMessage = null;
    westErrorMessage = null;
  }

  public void setEastLoadingStatus(boolean isLoading) {
    isEastLoading = isLoading;
  }

  public void setWestLoadingStatus(boolean isLoading) {
    isWestLoading = isLoading;
  }

  public void setEastErrorMessage(String eastErrorMessage) {
    this.eastErrorMessage = eastErrorMessage;
    eastTrams.clear();
  }

  public void setWestErrorMessage(String westErrorMessage) {
    this.westErrorMessage = westErrorMessage;
    westTrams.clear();
  }
}
