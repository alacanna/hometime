package com.aconex.hometime.api;

import com.aconex.hometime.api.response.BaseResponse;
import com.aconex.hometime.data.RouteData;
import com.aconex.hometime.data.TokenData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by amandalacanna on 8/31/17.
 */
public interface Repository {

  @GET("GetDeviceToken/?aid=TTIOSJSON&devInfo=TTIOSJSON")
  Call<BaseResponse<TokenData>> fetchAPIToken();

  @GET("GetNextPredictedRoutesCollection/{stopId}/0/false/?aid=TTIOSJSON&cid=2")
  Call<BaseResponse<RouteData>> fetchAPIRoutes(@Path("stopId") String stopId,
                                               @Query("tkn") String token );
}
