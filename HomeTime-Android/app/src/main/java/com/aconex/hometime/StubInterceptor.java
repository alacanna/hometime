package com.aconex.hometime;

import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static java.net.HttpURLConnection.HTTP_OK;

public class StubInterceptor implements Interceptor {

  private static final String API_DATE_FORMAT = "/Date(%d+1100)/";

  private static StubInterceptor interceptor;

  private List<FakeResponse> fakeResponses = new ArrayList<>();

  public static StubInterceptor getInstance() {
    if (interceptor == null) {
      interceptor = new StubInterceptor();

      interceptor.addResponse(new FakeResponse(".*GetDeviceToken.*",
        HTTP_OK,
        "{\"responseObject\":[{\"DeviceToken\":\"some-token\"}]}\n", null));

      interceptor.addResponse(new FakeResponse(".*GetNextPredictedRoutesCollection.*3304.*",
        HTTP_OK,
        String.format(Locale.ENGLISH,
          "{\"responseObject\":[{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}" +
            ",{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}]}\n",
          getMinutesFromNowDateString(-60),
          getMinutesFromNowDateString(-5),
          getMinutesFromNowDateString(5),
          getMinutesFromNowDateString(24 * 60 - 10),
          getMinutesFromNowDateString(24 * 60 + 10)), null));

      interceptor.addResponse(new FakeResponse(".*GetNextPredictedRoutesCollection.*3204.*",
        HTTP_OK,
        String.format(Locale.ENGLISH,
          "{\"responseObject\":[{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}" +
            ",{\"PredictedArrivalDateTime\":\"%s\"},{\"PredictedArrivalDateTime\":\"%s\"}]}\n",
          getMinutesFromNowDateString(-1),
          getMinutesFromNowDateString(90),
          getMinutesFromNowDateString(3 * 24 * 60),
          getMinutesFromNowDateString(7 * 24 * 60 + 10)), null));
    }

    return interceptor;
  }

  private StubInterceptor() {
  }

  @Override
  public Response intercept(Chain chain) throws IOException {
    String url = chain.request().url().toString();

    for (FakeResponse fakeResponse : fakeResponses) {
      Pattern pattern = Pattern.compile(fakeResponse.getPattern());
      Matcher matcher = pattern.matcher(url);
      if (matcher.find()) {
        delayRequestFor(1000);
        return createStubbedResponse(chain, fakeResponse);
      }
    }

    return chain.proceed(chain.request());
  }

  private void delayRequestFor(int millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException ignored) {
    }
  }

  public void addResponse(FakeResponse fakeResponse) {
    fakeResponses.add(fakeResponse);
  }

  public void clearResponses() {
    fakeResponses.clear();
  }

  private Response createStubbedResponse(Chain chain, FakeResponse fakeResponse) throws IOException {
    if (!TextUtils.isEmpty(fakeResponse.getErrorMessage())) {
      throw new IOException(fakeResponse.getErrorMessage());
    }

    return new Response.Builder()
      .code(fakeResponse.getStatusCode())
      .message(fakeResponse.getResponse())
      .request(chain.request())
      .protocol(Protocol.HTTP_2)
      .body(ResponseBody.create(MediaType.parse("application/json"), fakeResponse.getResponse().getBytes()))
      .addHeader("content-type", "application/json")
      .build();
  }

  public static String getMinutesFromNowDateString(int minutes) {
    Calendar calendar = Calendar.getInstance();

    calendar.set(Calendar.YEAR, 2016);
    calendar.set(Calendar.MONTH, 7);
    calendar.set(Calendar.DAY_OF_MONTH, 29);
    calendar.set(Calendar.HOUR_OF_DAY, 9);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);

    calendar.add(Calendar.MINUTE, minutes);
    return String.format(Locale.ENGLISH, API_DATE_FORMAT, calendar.getTimeInMillis());
  }

  public static class FakeResponse {

    private final String pattern;
    private final int statusCode;
    private final String response;
    private final String errorMessage;

    public FakeResponse(String pattern, int statusCode, String response, String errorMessage) {
      this.pattern = pattern;
      this.statusCode = statusCode;
      this.response = response;
      this.errorMessage = errorMessage;
    }

    public String getPattern() {
      return pattern;
    }

    public int getStatusCode() {
      return statusCode;
    }

    public String getResponse() {
      return response;
    }

    public String getErrorMessage() {
      return errorMessage;
    }
  }
}
