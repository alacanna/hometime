package com.aconex.hometime.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amandalacanna on 9/2/17.
 */

public class RouteData {
  @SerializedName("Destination")
  private String destination;
  @SerializedName("PredictedArrivalDateTime")
  private String predictedArrivalDateTime;
  @SerializedName("RouteNo")
  private int route_number;

  public RouteData() {
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getPredictedArrivalDateTime() {
    return predictedArrivalDateTime;
  }

  public void setPredictedArrivalDateTime(String predictedArrivalDateTime) {
    this.predictedArrivalDateTime = predictedArrivalDateTime;
  }

  public int getRoute_number() {
    return route_number;
  }

  public void setRoute_number(int route_number) {
    this.route_number = route_number;
  }
}
