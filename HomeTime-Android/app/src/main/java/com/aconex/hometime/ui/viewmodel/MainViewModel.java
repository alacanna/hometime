package com.aconex.hometime.ui.viewmodel;

/**
 * Created by amandalacanna on 9/2/17.
 */

public class MainViewModel {

  interface MainViewModelInputs {
  }

  interface MainViewModelOutputs {
  }

  interface MainViewModelType {
    MainViewModelInputs inputs();
    MainViewModelOutputs outputs();
  }


  class MainViewModelImpl extends MainViewModel implements MainViewModelInputs, MainViewModelOutputs, MainViewModelType {
    @Override
    public MainViewModelInputs inputs() {
      return this;
    }

    @Override
    public MainViewModelOutputs outputs() {
      return this;
    }

    public MainViewModelImpl() {

    }
  }
}
