package com.aconex.hometime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TramsDateConverter {

  private static final String PLACEHOLDER_TIME = "???";

  public static String formattedTimeFromString(String rawDateString) {
    Date dateFromString = dateFromString(rawDateString);
    if (dateFromString != null) {
      return new SimpleDateFormat("h:mm a", Locale.ENGLISH).format(dateFromString);
    }
    return PLACEHOLDER_TIME;
  }

  private static Date dateFromString(String rawDateString) {
    long timestamp = timestampFromString(rawDateString);
    if (timestamp != -1) {
      return new Date(timestamp);
    }
    return null;
  }

  private static long timestampFromString(String rawDateString) {
    int beginIndex = rawDateString.indexOf("(");
    int endIndex = Math.max(rawDateString.indexOf("+"), rawDateString.indexOf("-"));
    try {
      String substring = rawDateString.substring(beginIndex + 1, endIndex);
      return Long.parseLong(substring);
    } catch (Exception exception) {
      return -1;
    }
  }
}
