package com.aconex.hometime.api;

import com.aconex.hometime.BuildConfig;
import com.aconex.hometime.StubInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by amandalacanna on 8/31/17.
 */

public class RestAPI {

  private RestAPI() {
  }

  public static <S> S createService(Class<S> serviceClass) {

    OkHttpClient.Builder client = new OkHttpClient.Builder();

    if (BuildConfig.USE_NETWORK_STUB) {
      client.addInterceptor(StubInterceptor.getInstance());
    }

    return  new Retrofit.Builder()
      .baseUrl(BuildConfig.API_URL)
      .client(client.build())
      .addConverterFactory(GsonConverterFactory.create())
      .build()
      .create(serviceClass);
  }

}
