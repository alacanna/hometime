package com.aconex.hometime.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by amandalacanna on 8/31/17.
 */

public class TokenData {
  @SerializedName("DeviceToken")
  private String deviceToken;

  public TokenData() {
  }

  public String getDeviceToken() {
    return deviceToken;
  }

  public void setDeviceToken(String deviceToken) {
    this.deviceToken = deviceToken;
  }
}
