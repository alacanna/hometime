package com.aconex.hometime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.aconex.hometime.api.Repository;
import com.aconex.hometime.api.RestAPI;
import com.aconex.hometime.api.response.BaseResponse;
import com.aconex.hometime.data.RouteData;
import com.aconex.hometime.data.TokenData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

  private static final String EAST_STOP_ID = "3304";
  private static final String WEST_STOP_ID = "3204";

  private final MainAdapter adapter = new MainAdapter(this);
  private String deviceToken;

  private Repository repository;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    repository = RestAPI.createService(Repository.class);
    initialiseRecyclerView();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_load:
        loadTramData();
        return true;
      case R.id.menu_clear:
        clearTramData();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void initialiseRecyclerView() {
    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(adapter);
  }

  private void clearTramData() {
    adapter.clearAllData();
    adapter.notifyDataSetChanged();
  }

  private void loadTramData() {
    adapter.clearAllData();
    adapter.setEastLoadingStatus(true);
    adapter.setWestLoadingStatus(true);
    adapter.notifyDataSetChanged();

    if (deviceToken == null) {
      fetchAPIToken(new DeviceTokenCallback() {

        @Override
        public void onSuccess(String token) {
          deviceToken = token;
          loadTramDataUsingToken(deviceToken);
        }

        @Override
        public void onFailure(String errorMessage) {
          deviceToken = null;
          adapter.clearAllData();
          adapter.setEastErrorMessage(errorMessage);
          adapter.setWestErrorMessage(errorMessage);
          adapter.notifyDataSetChanged();
        }
      });
    } else {
      loadTramDataUsingToken(deviceToken);
    }
  }

  private interface DeviceTokenCallback {
    void onSuccess(String deviceToken);
    void onFailure(String errorMessage);
  }

  private void fetchAPIToken(final DeviceTokenCallback callback) {
    repository.fetchAPIToken().enqueue(new Callback<BaseResponse<TokenData>>() {
      @Override
      public void onResponse(Call<BaseResponse<TokenData>> call, Response<BaseResponse<TokenData>> response) {
        BaseResponse<TokenData> responseBody = response.body();
        List<TokenData> tokenDataList = responseBody.getResponseObject();
        String token = tokenDataList.get(0).getDeviceToken();

        if (token == null || token.isEmpty()) {
          callback.onFailure(getString(R.string.something_went_wrong_error));
        } else {
          callback.onSuccess(token);
        }
        callback.onSuccess(token);
      }

      @Override
      public void onFailure(Call<BaseResponse<TokenData>> call, Throwable t) {
        callback.onFailure(getString(R.string.something_went_wrong_error));
      }
    });
  }


  private void loadTramDataUsingToken(String token) {

    getWestRoute(token);
    getEastRoute(token);

  }

  private void getEastRoute(String token) {
    repository.fetchAPIRoutes(WEST_STOP_ID, token).enqueue(new Callback<BaseResponse<RouteData>>() {
      @Override
      public void onResponse(Call<BaseResponse<RouteData>> call, Response<BaseResponse<RouteData>> response) {

        if(response.body().getResponseObject() != null){
          List<RouteData> listRoutes = response.body().getResponseObject();
          ArrayList<String> upcomingTrams = new ArrayList<>(listRoutes.size());

          for (RouteData routeData : listRoutes){
            upcomingTrams.add(routeData.getPredictedArrivalDateTime());
          }

          adapter.setEastLoadingStatus(false);
          adapter.setEastErrorMessage(null);
          adapter.setEastTrams(upcomingTrams);
          adapter.notifyDataSetChanged();
        }else {
          adapter.setEastLoadingStatus(false);
          adapter.setEastErrorMessage(getErrorMessage(new Throwable("List is null")));
          adapter.notifyDataSetChanged();
        }

      }

      @Override
      public void onFailure(Call<BaseResponse<RouteData>> call, Throwable t) {
        adapter.setEastLoadingStatus(false);
        adapter.setEastErrorMessage(getErrorMessage(t));
        adapter.notifyDataSetChanged();
      }
    });
  }

  private void getWestRoute(String token) {
    repository.fetchAPIRoutes(EAST_STOP_ID,token).enqueue(new Callback<BaseResponse<RouteData>>() {
      @Override
      public void onResponse(Call<BaseResponse<RouteData>> call, Response<BaseResponse<RouteData>> response) {

        if(response.body().getResponseObject() != null){
          List<RouteData> listRoutes = response.body().getResponseObject();
          ArrayList<String> upcomingTrams = new ArrayList<>(listRoutes.size());

          for (RouteData routeData : listRoutes){
            upcomingTrams.add(routeData.getPredictedArrivalDateTime());
          }

          adapter.setWestLoadingStatus(false);
          adapter.setWestErrorMessage(null);
          adapter.setWestTrams(upcomingTrams);
          adapter.notifyDataSetChanged();
        }else {
          adapter.setWestLoadingStatus(false);
          adapter.setWestErrorMessage(getErrorMessage(new Throwable("List is null")));
          adapter.notifyDataSetChanged();
        }

      }

      @Override
      public void onFailure(Call<BaseResponse<RouteData>> call, Throwable t) {
        adapter.setWestLoadingStatus(false);
        adapter.setWestErrorMessage(getErrorMessage(t));
        adapter.notifyDataSetChanged();
      }
    });
  }

  private String getErrorMessage(Throwable throwable) {
    return TextUtils.isEmpty(throwable.getMessage())
      ? getString(R.string.something_went_wrong_error) : throwable.getMessage();
  }
}
