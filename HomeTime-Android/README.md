# HomeTime

This repository contains a simple sample app using the Tram Tracker API to be used as a coding assignment for Aconex Android developer candidates. Rather than getting you to build an entire sample app for us, we'd like you to take a look at this little app, and have a go at improving and extending it.


## Existing Functionality

The app is hard coded to show the next upcoming trams going each direction on Bourke St near Queen St in the Melbourne CBD. There is a single table view with two sections, one for east (towards the Parliament end of the city) and one for west (towards the Southern Cross station end of the city).

* When the app loads, the table shows the empty state, that no timetable information has already been loaded
* When you tap 'Load', the app retrieves the upcoming trams from the API for both east and west and places the dates in the list
* If no upcoming trams are returned, a placeholder is displayed in the table
* If an error occurs, we display the error message inline in the table cells


## Tram Tracker API

This app uses the same API as the Tram Tracker app, but it's not an officially public API so there is a chance it'll just stop working at some stage. It's more fun to use a real API though. To use the tram tracker API, you need to first connect with an endpoint that gives you an API token. That token can then be used for future calls.

To retrieve an API token, you hit this endpoint `http://ws3.tramtracker.com.au/TramTracker/RestService/GetDeviceToken/?aid=TTIOSJSON&devInfo=TTIOSJSON` and retrieve the token from the response. The app id and dev info parameters have been coded in for you, as these should not need to change.

```
{
  errorMessage: null,
  hasError: false,
  hasResponse: true,
  responseObject: [
    {
      DeviceToken: "some-valid-device-token"
    }
  ]
}
```

We can then use this device token to retrieve the upcoming trams. The route ID and stop IDs that we pass to the API have been hard coded for you to represent the tram stops on either side of the road. The endpoint that retrieves the tram (with stop ID and token replaced with valid values) will be of the form `http://ws3.tramtracker.com.au/TramTracker/RestService/GetNextPredictedRoutesCollection/{STOP_ID}/0/false/?aid=TTIOSJSON&cid=2&tkn={TOKEN}`, returns the upcoming trams in the form:

```
{
  errorMessage: null,
  hasError: false,
  hasResponse: true,
  responseObject: [
    {
      Destination: "Bundoora RMIT",
      PredictedArrivalDateTime: "/Date(1469406216000+1000)/",
      RouteNo: "86"
    },
    {
      Destination: "East Brunswick",
      PredictedArrivalDateTime: "/Date(1469406660000+1000)/",
      RouteNo: "96"
    },
    {
      Destination: "Bundoora RMIT",
      PredictedArrivalDateTime: "/Date(1469407140000+1000)/",
      RouteNo: "86"
    }
  ]
}
```

The dates returned look like they're in a format based on an integer since 1970, rather than something friendly and widely used like ISO8601. There is some code in `TramsDateConverter.java` to convert these strings into `Date` objects that we can use in the app, which also has a bunch of unit tests documenting the conversion.

You'll notice it's one of those APIs that sometimes gives you error messages inside a valid JSON response. We ignore this for now and assume that a `200` response means that the data will be on the `responseObject` field. Either a network failure, or the inability to parse the correct data out of the `responseObject` is considered an error.


## Existing Android Code

Most of the code is contained in the `MainActivity.java` and `MainAdapter.java` classes. This is not meant to be an example of how code should be written, but rather an opportunity to think about better ways of breaking down and structuring code in a simple context.

The activity keeps the retrieved tram data in two list properties within the adapter, `eastTrams` and `westTrams`, which are initially empty. While the request is loading, this is tracked in two separate lifecycle properties `isLoadingEast` and `isLoadingWest`. Once the data is loaded, the lists of upcoming tram times are populated and displayed in the table, or if an error occurs the cause is displayed.

The tram tracker API requires a token to be attached to the request as a URL parameter Initially, there is no token, but once we have retrieved the token once, it is stored in the `deviceToken` property and used from then on. All network requests use the same `tramTrackerService` property. The creation of the `TramTrackerService` uses a build config flag to allow tests to inject a stub network handler with fake responses returned.

The `MainAdapter` uses the tram data, the loading state and the error state, to determine what to show in the table. There are always 2 sections, one representing each direction for the trams. If we have not retrieved any tram information, we show this in the table. While the request loads, we show this state in the table. Once we have retrieved tram data, we show this in the data in the table or the error message. If the request for tram data succeeds but has an empty list of upcoming trams, this will result in no rows being shown in the table.

The `loadTramData()` method checks whether we already have a token to use, or otherwise uses `fetchApiToken()` to call back once the token is retrieved. We then use `loadTramDataUsingToken` to fetch both the east and west tram data. The URLs are hard coded, with the `stopId` for each direction passed in.


## Testing

The *development* and *production* flavours run against a live API while the *stub* flavour sets a build config option to make the app run against a dynamic stub that generates some trams we've just missed, some upcoming and some far into the future. The available build config arguments to set for testing are:

* USE_NETWORK_STUB: whether to hit the real API or use a dynamic stub

Most of the sample code is not properly unit tested, but a couple of tests exist to try and verify the expected functionality for you. The date formatter has some tests covering the expected string formatting, using standard JUnit.

There is also a sample UI test. This boots up the app and tests the loading and clearing of the UI, as well as any error states. Remember to *use the stub flavour* if you want the UI tests to pass, because they rely on this hard coded data.

## Coding Task

### Refactoring

The functionality is mostly complete, but the code isn't very maintainable and as features are added, having all the code in the activity isn't going to be maintainable. We would like you to look at some ways of improving the code quality, to make it easier to maintain and easier to test.

This task should only take a couple of hours. Don't feel like you have to fix every issue you see in the code, but tell us some of the things you would like to fix, what you think the major problems are and have a go at restructuring the app to solve this.

You can add unit tests as appropriate. The existing `TramsDateConverter.java` is an example of some functionality that doesn't belong in the activity that has been split into a separate piece of code, with unit tests to make sure it works and as examples of how it works.

### Extending

With a better code structure in place, try adding the following couple small pieces of functionality. Don't treat any of the existing code as sacrosanct, feel free to adapt, delete and rewrite as necessary. The high level tests driving the lifecycle of the activity should verify the app is still functioning as expected.

**Part 1:** Instead of just showing the time for the next tram, also show how far away that is from the current time, according to the following rules:

* If a time is in the past, only show the time without any addition, eg. 8:59 AM
* If a time is in the next hour, show the upcoming time in minutes, eg. 9:05 AM, in 5 min *or* 9:50 AM, in 50 min
* If a time is within next day, show the upcoming time in hours, eg. 10:15 AM, in 1 hour *or* 8:15 AM, in 23 hours
* If a time is beyond 24 hours, show the time and the day of the week. eg. 9:05 AM, Wednesday

**Part 2:** Strip or highlight tram arrival times that are in the past:

* If a time is in the past, but within the last 10 minutes, show the text in *grey* instead of black
* If a time is more than 10 minutes ago, do not show those arrival times at all
* If a time is more than 7 days in the future, do not show those arrival times at all

There is a commented out UI test `appShowsFormattedTramTimesAfterLoading` that describes the expected output on the screen one these extensions have been made.


## Feedback

We're always trying to improve our hiring process. This task is designed for you to be able to show your Android development skills without spending a huge amount of time on it. We welcome any feedback on how this could be improved.

Please send any feedback to Stewart Gleadow, sgleadow@aconex.com or Jacky Li, jackyl@aconex.com
